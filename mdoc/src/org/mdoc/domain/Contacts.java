package org.mdoc.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@XmlRootElement(name = "contacts")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "contacts")
public class Contacts implements Serializable, Iterable<Contact> {

	@XmlElement(name = "contact")
	@JsonProperty
	private List<Contact> contacts = new ArrayList<>();

	public Contacts() {
	}

	public Contacts(List<Contact> c) {
		contacts.addAll(c);
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts.addAll(contacts);
	}

	public int size() {
		return contacts.size();
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	@Override
	public Iterator<Contact> iterator() {
		return contacts.iterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contacts == null) ? 0 : contacts.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contacts other = (Contacts) obj;
		if (contacts == null) {
			if (other.contacts != null)
				return false;
		} else if (!contacts.equals(other.contacts))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return contacts.toString();
	}
}
