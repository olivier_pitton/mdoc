package org.mdoc.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;


@XmlRootElement(name = "entreprise")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value="entreprise")
public class Entreprise extends Contact {

	@XmlElement
	@JsonProperty
	private int numSiret;

	public Entreprise() {
	}

	public int getNumSiret() {
		return numSiret;
	}

	public void setNumSiret(int numSiret) {
		this.numSiret = numSiret;
	}

	@Override
	public String format() {
		StringBuilder sb = new StringBuilder(super.format());
		sb.append(" - ").append(numSiret);
		return sb.toString();
	}

}
