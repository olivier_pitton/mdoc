package org.mdoc.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@XmlRootElement(name = "groups")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "groups")
public class Groups implements Serializable, Iterable<GroupContact> {

	@XmlElement(name = "group")
	@JsonProperty
	private List<GroupContact> groups = new ArrayList<>();

	public Groups() {
	}

	public Groups(List<GroupContact> groups) {
		this.groups.addAll(groups);
	}

	public List<GroupContact> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupContact> groups) {
		this.groups.addAll(groups);
	}

	@Override
	public Iterator<GroupContact> iterator() {
		return groups.iterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((groups == null) ? 0 : groups.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Groups other = (Groups) obj;
		if (groups == null) {
			if (other.groups != null)
				return false;
		} else if (!groups.equals(other.groups))
			return false;
		return true;
	}
}
