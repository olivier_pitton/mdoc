package org.mdoc.domain;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.mdoc.util.IFormattable;
import org.mdoc.util.IWithName;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@XmlRootElement(name = "group")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "group")
public class GroupContact implements Serializable, Comparable<GroupContact>, IWithName, IFormattable {

	@XmlElement
	@JsonProperty
	private int groupId;
	@XmlElement
	@JsonProperty
	private String groupName;
	@JsonProperty
	@XmlElement(nillable = true)
	private List<Contact> contact;
	@XmlTransient
	@JsonIgnore
	private long version;

	public GroupContact() {
	}

	public GroupContact(String groupName) {
		this.groupName = groupName;
	}

	public GroupContact(String groupName, List<Contact> contact) {
		this.groupName = groupName;
		this.contact = contact;
	}

	@Override
	public long getVersion() {
		return version;
	}

	@Override
	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public int getId() {
		return getGroupId();
	}

	@Override
	public void setId(int id) {
		setGroupId(id);
	}

	@Override
	public String getName() {
		return groupName;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Contact> getContact() {
		return contact;
	}

	public void setContact(List<Contact> contact) {
		this.contact = contact;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + groupId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupContact other = (GroupContact) obj;
		if (groupId != other.groupId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return groupName;
	}

	@Override
	public int compareTo(GroupContact o) {
		return (o == null) ? -1 : groupId - o.groupId;
	}

	@Override
	public String format() {
		return groupId + " - " + groupName;
	}

}
