package org.mdoc.domain;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.mdoc.util.IFormattable;
import org.mdoc.util.IWithName;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@XmlRootElement(name = "contact")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "contact")
public class Contact implements Serializable, Comparable<Contact>, IWithName, IFormattable {

	@XmlElement
	@JsonProperty
	private int id;
	@XmlElement
	@JsonProperty
	private String firstName;
	@XmlElement
	@JsonProperty
	private String lastName;
	@XmlElement
	@JsonProperty
	private String email;
	@XmlTransient
	@JsonIgnore
	private List<GroupContact> groups;
	@XmlElement(nillable = true, name = "address")
	@JsonProperty(value = "address")
	private Address add;
	@XmlElement(nillable = true, name = "phones")
	@JsonProperty(value = "phones")
	private List<PhoneNumber> profiles;



	@XmlTransient
	@JsonIgnore
	private long version;

	public Contact() {
	}

	public Contact(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	@Override
	public long getVersion() {
		return version;
	}

	@Override
	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return getLastName();
	}

	public Address getAdd() {
		return add;
	}

	public void setAdd(Address add) {
		this.add = add;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<GroupContact> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupContact> groups) {
		this.groups = groups;
	}

	public List<PhoneNumber> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<PhoneNumber> profiles) {
		this.profiles = profiles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.lastName + " " + this.firstName;
	}

	@Override
	public int compareTo(Contact o) {
		if (o == null)
			return -1;
		return this.id - o.id;
	}

	@Override
	public String format() {
		StringBuilder sb = new StringBuilder();
		sb.append(id).append(" - ");
		sb.append(lastName).append(" - ");
		sb.append(firstName).append(" - ");
		sb.append(email);
		return sb.toString();
	}

}
