package org.mdoc.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.mdoc.util.IFormattable;
import org.mdoc.util.IWithName;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@XmlRootElement(name = "address")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "address")
public class Address implements Serializable, Comparable<Address>, IWithName, IFormattable {

	@XmlElement
	@JsonProperty
	private int id;
	@XmlElement(nillable = true)
	@JsonProperty
	private String street;
	@XmlElement(nillable = true)
	@JsonProperty
	private String city;
	@XmlElement(nillable = true)
	@JsonProperty
	private String zip;
	@XmlElement(nillable = true)
	@JsonProperty
	private String country;
	@XmlTransient
	@JsonIgnore
	private long version;

	public Address() {
	}

	public Address(String street, String city, String zip, String country) {
		this.street = street;
		this.city = city;
		this.zip = zip;
		this.country = country;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return format();
	}

	@Override
	public long getVersion() {
		return version;
	}

	@Override
	public void setVersion(long version) {
		this.version = version;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public int compareTo(Address o) {
		return (o == null) ? -1 : id - o.id;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}

	@Override
	public String format() {
		StringBuilder sb = new StringBuilder();
		sb.append(id).append(" - ");
		sb.append(street).append(" - ");
		sb.append(city).append(" - ");
		sb.append(zip).append(" - ");
		sb.append(country);
		return sb.toString();
	}
}
