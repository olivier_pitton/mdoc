package org.mdoc.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.mdoc.util.IFormattable;
import org.mdoc.util.IWithName;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@XmlRootElement(name = "phone")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "phone")
public class PhoneNumber implements Serializable, Comparable<PhoneNumber>, IWithName, IFormattable {

	@XmlElement
	@JsonProperty
	private int id;
	@XmlElement(nillable = true)
	@JsonProperty
	private String phoneKind;
	@XmlElement
	@JsonProperty
	private String phoneNumber;
	@XmlTransient
	@JsonIgnore
	private Contact contact;
	@XmlTransient
	@JsonIgnore
	private long version;

	public PhoneNumber() {
	}

	public PhoneNumber(String phoneKind, String phoneNumber) {
		this.phoneKind = phoneKind;
		this.phoneNumber = phoneNumber;
	}

	@Override
	public long getVersion() {
		return version;
	}

	@Override
	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return getPhoneNumber();
	}

	public String getPhoneKind() {
		return phoneKind;
	}

	public void setPhoneKind(String phoneKind) {
		this.phoneKind = phoneKind;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneNumber other = (PhoneNumber) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.phoneNumber;
	}

	@Override
	public int compareTo(PhoneNumber o) {
		return (o == null) ? -1 : id - o.id;
	}

	@Override
	public String format() {
		StringBuilder sb = new StringBuilder();
		sb.append(id).append(" - ");
		sb.append(phoneKind).append(" - ");
		sb.append(phoneNumber);
		if (contact != null) {
			sb.append(" - ").append(contact.getEmail());
		}
		return sb.toString();
	}

}
