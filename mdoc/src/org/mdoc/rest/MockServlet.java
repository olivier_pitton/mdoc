package org.mdoc.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mdoc.dao.DAO;
import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@WebServlet(name="mock", loadOnStartup=1, urlPatterns={"/faces/mock"})
public class MockServlet extends HttpServlet {

	static private boolean isInit = false;
	
	@SuppressWarnings("unchecked")
	@Override
	protected synchronized void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(isInit) {
			return;
		}
		ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		List<Contact> contacts = new ArrayList<>(4);
		try(DAO<Contact> dao = (DAO<Contact>) context.getBean("contactDao")) {
			contacts.add(dao.add(new Contact("Wandrille", "Domin", "wandrille.domin@gmail.com")));
			Contact olivier = new Contact("Olivier", "Pitton", "olivier.pitton@gmail.com");
			olivier.setAdd(new Address("87 rue de l'Avenir",  "Vanves", "92170", "France"));
			contacts.add(dao.add(olivier));
			contacts.add(dao.add(new Contact("Jean", "Ly", "jean.ly@gmail.com")));
			contacts.add(dao.add(new Contact("Christophe", "Julien", "chris.julien@gmail.com")));
		}
		try(DAO<GroupContact> dao = (DAO<GroupContact>) context.getBean("groupContactDao")) {
			dao.add(new GroupContact("mdoc", Arrays.asList(contacts.get(0), contacts.get(1))));
			dao.add(new GroupContact("eter", Arrays.asList(contacts.get(2))));
			dao.add(new GroupContact("vfsr", Arrays.asList(contacts.get(3))));
		}
		isInit = true;
	}

}
