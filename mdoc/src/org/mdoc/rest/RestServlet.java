package org.mdoc.rest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mdoc.dao.DAO;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Contacts;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.Groups;
import org.mdoc.exporter.Exporter;
import org.mdoc.exporter.ExporterFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@WebServlet(name = "rest", urlPatterns = "/faces/api", loadOnStartup = 1)
public class RestServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String type = req.getParameter("type");
		if (type == null) {
			type = "contact";
		}
		String format = req.getParameter("format");
		if (format == null) {
			format = "json";
		}
		WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		Exporter exporter = applicationContext.getBean(ExporterFactory.class).create(format);
		if (type.equals("contact")) {
			try {
				exportContacts(resp, exporter, applicationContext);
			} catch (Exception e) {
				resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		} else if (type.equals("group")) {
			try {
				exportGroups(resp, exporter, applicationContext);
			} catch (Exception e) {
				resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}

		} else {
			resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}

	@SuppressWarnings("unchecked")
	private void exportContacts(HttpServletResponse response, Exporter exporter, ApplicationContext context) throws Exception {
		try (DAO<Contact> dao = (DAO<Contact>) context.getBean("contactDao")) {
			Contacts c = context.getBean(Contacts.class);
			c.setContacts(dao.findAll());
			String res = exporter.exportData(c);
			response.getWriter().print(res);
			response.setContentType(exporter.getType());
		}
	}

	@SuppressWarnings("unchecked")
	private void exportGroups(HttpServletResponse response, Exporter exporter, ApplicationContext context) throws Exception {
		try (DAO<GroupContact> dao = (DAO<GroupContact>) context.getBean("groupContactDao")) {
			Groups gc = context.getBean(Groups.class);
			gc.setGroups(dao.findAll());
			String res = exporter.exportData(gc);
			response.getWriter().print(res);
			response.setContentType(exporter.getType());
		}
	}

}
