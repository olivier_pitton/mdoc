package org.mdoc.exporter;

public class ExporterFactory {

	public Exporter createXml() {
		return new XmlExporter();
	}

	public Exporter createJson() {
		return new JsonExporter();
	}

	public Exporter create(String format) {
		if(format.equals("xml")) {
			return createXml();
		} else if(format.equals("json")) {
			return createJson();
		}
		return null;
	}
	
}
