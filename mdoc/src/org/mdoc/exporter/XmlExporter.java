package org.mdoc.exporter;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Contacts;
import org.mdoc.domain.Groups;
import org.mdoc.domain.Entreprise;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;

public class XmlExporter implements Exporter {

	static private final JAXBContext context = init();

	static private JAXBContext init() {
		try {
			return JAXBContext.newInstance(Address.class, Entreprise.class,
					Contact.class, PhoneNumber.class, GroupContact.class,
					Contacts.class, Groups.class);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	private final Marshaller marshaller;

	public XmlExporter() {
		try {
			marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String exportData(Object ob) throws Exception {
		try (StringWriter writer = new StringWriter()) {
			marshaller.marshal(ob, writer);
			return writer.toString();
		}
	}

	@Override
	public String getType() {
		return "text/xml";
	}
	
	@Override
	public String getExtension() {
		return "xml";		
	}
}
