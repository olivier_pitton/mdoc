package org.mdoc.exporter;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonExporter implements Exporter {

	private final ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public String exportData(Object ob) throws Exception {
		return mapper.writer().withDefaultPrettyPrinter().writeValueAsString(ob);
	}

	@Override
	public String getType() {
		return "application/json";
	}

	@Override
	public String getExtension() {
		return "json";
	}

}
