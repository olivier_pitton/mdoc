package org.mdoc.exporter;

import org.mdoc.util.IExtensionnable;

/**
 * L'interface commune à tous les objets capable d'exporter des données.
 * @author pitton
 *
 */
public interface Exporter extends IExtensionnable {

	String exportData(Object ob) throws Exception;
		
}
