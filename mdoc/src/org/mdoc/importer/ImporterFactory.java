package org.mdoc.importer;

public class ImporterFactory {

	public ImporterFactory() {
	}

	public Importer createJson() {
		return new JsonImporter();
	}

	public Importer createXml() {
		return new XmlImporter();
	}

	public Importer createLinkedIn() {
		return new LinkedInImporter();
	}

}
