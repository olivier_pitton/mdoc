package org.mdoc.importer;

import org.mdoc.util.IExtensionnable;

public interface Importer extends IExtensionnable {

	Object importData(String val, Class<?> clazz) throws Exception;
	
}
