package org.mdoc.importer;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Contacts;
import org.mdoc.domain.Entreprise;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;

public class XmlImporter implements Importer {

	static private Unmarshaller init() {
		try {
			return JAXBContext.newInstance(Address.class, Entreprise.class,
					Contact.class, PhoneNumber.class, GroupContact.class,
					Contacts.class).createUnmarshaller();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	private Unmarshaller unmarshaller = init();
	
	XmlImporter() {
		
	}
	
	@Override
	public Object importData(String val, Class<?> clazz) throws Exception {
		try (StringReader reader = new StringReader(val)) {
			return unmarshaller.unmarshal(reader);
		}
	}

	@Override
	public String getType() {
		return "text/xml";
	}
	
	@Override
	public String getExtension() {
		return "xml";		
	}

}
