package org.mdoc.importer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonImporter implements Importer {

	private final ObjectMapper mapper = new ObjectMapper();
	
	public JsonImporter() {
	}

	@Override
	public Object importData(String val, Class<?> clazz) throws Exception {
		return mapper.readValue(val, clazz);
	}

	@Override
	public String getType() {
		return "application/json";
	}

	@Override
	public String getExtension() {
		return "json";
	}


}
