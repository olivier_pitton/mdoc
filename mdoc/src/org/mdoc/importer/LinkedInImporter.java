package org.mdoc.importer;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.mdoc.domain.Contacts;
import org.mdoc.social.model.LinkedInConnection;
import org.mdoc.social.model.LinkedInLocation;
import org.mdoc.social.model.LinkedInUser;

public class LinkedInImporter implements Importer {

	static private Unmarshaller init() {
		try {
			JAXBContext context = JAXBContext.newInstance(LinkedInUser.class,
					LinkedInLocation.class, LinkedInConnection.class);
			return context.createUnmarshaller();
		} catch (JAXBException e) {
		}
		return null;
	}

	private Unmarshaller unmarshaller = init();

	LinkedInImporter() {
		
	}
	
	@Override
	public Contacts importData(String val, Class<?> clazz) throws Exception {
		try (StringReader r = new StringReader(val)) {
			LinkedInConnection c = (LinkedInConnection) unmarshaller.unmarshal(r);
			return new Contacts(c.generateContacts());
		}
	}

	@Override
	public String getType() {
		return "linkedin";
	}

	@Override
	public String getExtension() {
		return "xml";
	}

}
