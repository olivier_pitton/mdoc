package org.mdoc.finder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.mdoc.util.IWithName;

public class LuceneFinder implements Finder {

	static private final Version version = Version.LUCENE_46;
	private Directory directory;
	private Analyzer analyzer;

	public LuceneFinder() {
	}

	@Override
	public Finder init() throws IOException {
		directory = new RAMDirectory();
		analyzer = new StandardAnalyzer(version);
		return this;
	}

	@Override
	public List<SearchResult> find(String request) throws IOException, ParseException {
		return find(request, 25);
	}
	
	@Override
	public List<SearchResult> find(String request, int nb) throws IOException, ParseException {
		try (DirectoryReader open = DirectoryReader.open(directory)) {
			IndexSearcher searcher = new IndexSearcher(open);
			QueryParser parser = new QueryParser(version, "name", analyzer);
			parser.setAllowLeadingWildcard(true);
			Query query = parser.parse(request);			
			TopDocs search = searcher.search(query, nb);
			ScoreDoc[] res = search.scoreDocs;
			List<SearchResult> names = new ArrayList<>(res.length);
			for (int i = 0; i < res.length; i++) {
				Document doc = searcher.doc(res[i].doc);
				names.add(convert(doc));
			}
			return names;
		}
	}
	
	@Override
	public List<SearchResult> findAll() throws IOException {
		List<SearchResult> documents = new ArrayList<>();
		try (DirectoryReader open = DirectoryReader.open(directory)) {
			int max = open.maxDoc();
			for(int i = 0 ; i < max ; i++) {
				documents.add(convert(open.document(i)));
			}
		}
		return documents;
	}

	@Override
	public void index(IWithName object) throws IOException {
		try (IndexWriter writer = new IndexWriter(directory, new IndexWriterConfig(version, analyzer))) {
			writer.addDocument(convert(object));
			writer.commit();
		}
	}

	@Override
	public void close() throws IOException {
		analyzer.close();
		directory.close();
	}

	private Document convert(IWithName name) {
		Document doc = new Document();
		doc.add(new IntField("id", name.getId(), Store.YES));
		doc.add(new StringField("name", name.getName(), Store.YES));
		doc.add(new StringField("type", name.getClass().getSimpleName(), Store.YES));
		return doc;
	}
	
	private SearchResult convert(Document document) {
		return new SearchResult(document.getField("id").numericValue().intValue(), document.get("name"), document.get("type"));
	}
}