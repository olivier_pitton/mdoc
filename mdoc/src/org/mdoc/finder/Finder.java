package org.mdoc.finder;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.mdoc.util.IWithName;

public interface Finder extends Closeable {

	Finder init() throws IOException;
	
	List<SearchResult> find(String query) throws IOException, ParseException;
	
	List<SearchResult> find(String query, int nb) throws IOException, ParseException;
	
	List<SearchResult> findAll() throws IOException;
	
	void index(IWithName object) throws IOException;
	
}
