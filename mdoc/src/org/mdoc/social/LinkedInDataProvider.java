package org.mdoc.social;

import java.net.Proxy;

import org.mdoc.domain.Contacts;
import org.mdoc.importer.LinkedInImporter;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;
import org.springframework.context.ApplicationContext;

public class LinkedInDataProvider extends AbstractDataProvider {

	LinkedInDataProvider() {
	}

	@Override
	public LinkedInDataProvider build(Proxy proxy) {
		this.proxy = proxy;
		service = new ServiceBuilder().provider(LinkedInApi.class).apiKey("c71kxd6qbwp1").proxy(this.proxy).apiSecret("bamB5HmJj33X6ofC").build();
		requestToken = service.getRequestToken();
		url = service.getAuthorizationUrl(requestToken);
		return this;
	}

	@Override
	public String send() {
		OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.linkedin.com/v1/people/~/connections");
		service.signRequest(accessToken, request);
		request.setProxy(proxy);
		Response response = request.send();
		if(false == response.isSuccessful()) {
			return null;
		}
		return response.getBody();
	}

	@Override
	public Contacts getContacts(ApplicationContext context, String content) throws Exception {
		if(content == null) {
			return null;
		}
		return context.getBean("linkedinImporter", LinkedInImporter.class).importData(content, Contacts.class);
	}
}
