package org.mdoc.social.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.mdoc.domain.Contact;

@XmlRootElement(name = "connections")
@XmlAccessorType(XmlAccessType.FIELD)
public class LinkedInConnection implements Serializable {

	@XmlElement(name = "person")
	private List<LinkedInUser> person;

	public List<LinkedInUser> getPerson() {
		return person;
	}

	public void setPerson(List<LinkedInUser> person) {
		this.person = person;
	}
	
	public List<Contact> generateContacts() {
		if(person == null) {
			return Collections.emptyList();
		}
		List<Contact> contacts = new ArrayList<>(person.size());
		for(LinkedInUser user : person) {
			// Email not provided by LinkedIn API ~~
			Contact c = new Contact(user.getFirstname(), user.getLastname(), "");
			contacts.add(c);
		}
		return contacts;
	}

}
