package org.mdoc.social;

import java.net.InetSocketAddress;
import java.net.Proxy;

import org.mdoc.domain.Contacts;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.springframework.context.ApplicationContext;

public abstract class AbstractDataProvider implements DataProvider {

	OAuthService service;
	Token requestToken, accessToken;
	String url;
	Proxy proxy;

	protected AbstractDataProvider() {
	}

	@Override
	public DataProvider build() {
		return build(Proxy.NO_PROXY);
	}

	@Override
	public DataProvider build(String host, int port) {
		return build(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port)));
	}

	@Override
	public String getAuthorizationUrl() {
		return url;
	}

	@Override
	public void setVerifier(String verifier) {
		accessToken = service.getAccessToken(requestToken, new Verifier(verifier));
	}

	@Override
	public Contacts getContacts(ApplicationContext context) throws Exception {
		return getContacts(context, send());
	}
}
