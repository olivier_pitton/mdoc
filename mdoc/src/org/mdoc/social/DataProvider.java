package org.mdoc.social;

import java.net.Proxy;

import org.mdoc.domain.Contacts;
import org.springframework.context.ApplicationContext;

public interface DataProvider {

	DataProvider build();
	
	DataProvider build(String host, int port);
	
	DataProvider build(Proxy proxy);
	
	String getAuthorizationUrl();
	
	void setVerifier(String verifier);
	
	String send();
	
	Contacts getContacts(ApplicationContext context) throws Exception;
	
	Contacts getContacts(ApplicationContext context, String content) throws Exception;
}
