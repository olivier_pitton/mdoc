package org.mdoc.social;

public class DataProviderFactory {

	public DataProviderFactory() {
	}
	
	public DataProvider createLinkedIn() {
		return new LinkedInDataProvider();
	}

}
