package org.mdoc.dao;

import org.hibernate.SessionFactory;
import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Entreprise;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;
import org.mdoc.finder.Finder;
import org.mdoc.finder.LuceneFinder;
import org.mdoc.util.IFormattable;
import org.springframework.context.ApplicationContext;

public class DAOFactory {

	public DAOFactory() {
	}

	public DAO<Entreprise> createEntreprise() {
		return new EntrepriseDAO();
	}

	public DAO<Address> createAddress() {
		return new GlobalDAO<>(Address.class);
	}

	public DAO<PhoneNumber> createPhoneNumber() {
		return new PhoneNumberDAO();
	}

	public DAO<GroupContact> createGroupContact() {
		return new GroupContactDAO();
	}

	public DAO<Contact> createContact() {
		return new ContactDAO();
	}

	public DAO<? extends IFormattable> create(Class<?> clazz, ApplicationContext context) {
		SessionFactory factory = (SessionFactory) context.getBean("sessionFactory");
		Finder finder = context.getBean(LuceneFinder.class);
		DAO<? extends IFormattable> res = null;
		if (clazz == Contact.class) {
			res = createContact();
		} else if (clazz == PhoneNumber.class) {
			res = createPhoneNumber();
		} else if (clazz == GroupContact.class) {
			res = createGroupContact();
		} else if (clazz == Entreprise.class) {
			res = createEntreprise();
		} else if (clazz == Address.class) {
			res = createAddress();
		}
		res.setSessionFactory(factory);
		res.setFinder(finder);
		return res;
	}

}
