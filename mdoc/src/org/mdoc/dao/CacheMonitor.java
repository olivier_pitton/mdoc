package org.mdoc.dao;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;



import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class CacheMonitor {

  private final static Logger LOG = LoggerFactory.getLogger(CacheMonitor.class);
  private final static NumberFormat NF = new DecimalFormat("0.0###");

  private SessionFactory sessionFactory;

  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Around("execution(* org.mdoc.dao..*.*(..))")
  public Object logAround(ProceedingJoinPoint pjp) throws Throwable {
    Statistics statistics = sessionFactory.getStatistics();
    statistics.setStatisticsEnabled(true);

    long hit0 = statistics.getSecondLevelCacheHitCount();
    long miss0 = statistics.getSecondLevelCacheMissCount();
    long put0 = statistics.getSecondLevelCachePutCount();

    long cacheHit = statistics.getQueryCacheHitCount();
    long cacheMiss = statistics.getQueryCacheMissCount();
    long cachePut = statistics.getQueryCachePutCount();

    Object result = pjp.proceed();

    long hit1 = statistics.getSecondLevelCacheHitCount();
    long miss1 = statistics.getSecondLevelCacheMissCount();
    long put1 = statistics.getSecondLevelCachePutCount();

    long cacheHit1 = statistics.getQueryCacheHitCount();
    long cacheMiss1 = statistics.getQueryCacheMissCount();
    long cachePut1 = statistics.getQueryCachePutCount();

		if (put0 < put1) {
			LOG.info(String.format("Ajout de %d donnée(s) dans le cache L2. Signature=%s#%s", (put1 - put0), pjp.getTarget().getClass().getName(), pjp.getSignature().getName()));
			return result;
		}
		if(cachePut < cachePut1) {
			LOG.info(String.format("Ajout de %d donnée(s) dans le cache L1. Signature=%s#%s", (cachePut1 - cachePut), pjp.getTarget().getClass().getName(), pjp.getSignature().getName()));
			return result;
		}

    double ratioHit = 0.0;
    double ratioL1 = 0.0;
    if (hit1 + miss1 != 0) {
      ratioHit = (double) (hit1 / (hit1 + miss1));
    }
    if (cacheHit1 + cacheMiss1 != 0) {
      ratioL1 = (double) (cacheHit1 / (cacheHit1 + cacheMiss1));
    }


		if (hit1 > hit0) {
			LOG.info(String.format("Hit dans le cache L2. Ratio=%s - Signature=%s#%s", NF.format(ratioHit), pjp.getTarget().getClass().getName(), pjp.getSignature().getName()));
		} else if (miss1 > miss0) {
			LOG.info(String.format("Miss dans le cache L2. Ratio=%s - Signature=%s#%s", NF.format(ratioHit), pjp.getTarget().getClass().getName(), pjp.getSignature().getName()));
		} else if (cacheHit1 > cacheHit) {
			LOG.info(String.format("Hit dans le cache L1. Ratio=%s - Signature=%s#%s", NF.format(ratioL1), pjp.getTarget().getClass().getName(), pjp.getSignature().getName()));
		} else if (cacheMiss1 > cacheMiss) {
			LOG.info(String.format("Miss dans le cache L1. Ratio=%s - Signature=%s#%s", NF.format(ratioL1), pjp.getTarget().getClass().getName(), pjp.getSignature().getName()));
		} else {
			LOG.info("Cache non utilisé - Signature=%s#%s", pjp.getTarget().getClass().getName(), pjp.getSignature().getName());
		}
		return result;
	}

}
