package org.mdoc.dao;

import org.hibernate.Transaction;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;

public class ContactDAO extends GlobalDAO<Contact> {

  protected ContactDAO() {
    super(Contact.class);
  }

  @Override
  public Contact add(Contact object) {
    Transaction tr = getSession().beginTransaction();
    tr.begin();
    if (object.getAdd() != null) {
      getSession().saveOrUpdate(object.getAdd());
      index(object.getAdd());
    }
    if (object.getGroups() != null) {
      for (GroupContact group : object.getGroups()) {
        getSession().saveOrUpdate(group);
        index(group);
      }
    }
    if (object.getProfiles() != null) {
      for (PhoneNumber n : object.getProfiles()) {
      	getSession().saveOrUpdate(n);
        index(n);
      }
    }
    getSession().save(object);
    getSession().flush();
    tr.commit();
    index(object);
    return object;
  }

}
