package org.mdoc.dao;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.mdoc.finder.Finder;
import org.mdoc.util.IWithName;

public class GlobalDAO<T extends Serializable & IWithName> implements DAO<T> {

	private Session session;
	private final Class<T> clazz;
	private Finder finder;
	
	protected GlobalDAO(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	@Override
	public void setFinder(Finder finder) {
		this.finder = finder;
	}
	
	public Finder getFinder() {
		return finder;
	}
	
	@Override
	public void setSessionFactory(SessionFactory factory) {
		session = factory.openSession();
	}
	
	protected Session getSession(){
		return session;
	}

	@Override
	public Class<T> getObjectClass() {
		return clazz;
	}

	protected void index(IWithName object) {
		try {
			finder.index(object);
		} catch (IOException e) {
		}
	}
	
	@Override
	public T add(T object) {
		Transaction tr = getSession().beginTransaction();
		tr.begin();
		getSession().save(object);
		tr.commit();
		index(object);
		return object;
	}

	@Override
	public void remove(T object) {
		Transaction tr = getSession().beginTransaction();
		tr.begin();
		getSession().delete(object);
		tr.commit();
	}

	@Override
	public T merge(T object) {
		Transaction tr = getSession().beginTransaction();
		tr.begin();
		getSession().update(object);
		tr.commit();
		return object;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T find(int id) {
		return (T) getSession().get(clazz, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {
		Query query = getSession().createQuery("FROM " + clazz.getName());
		query.setCacheable(true);
		return query.list();
	}

	@Override
	public void close() {
		getSession().flush();
		getSession().close();
	}

}
