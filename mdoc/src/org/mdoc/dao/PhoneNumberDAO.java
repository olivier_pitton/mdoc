package org.mdoc.dao;

import org.hibernate.Transaction;
import org.mdoc.domain.PhoneNumber;

public class PhoneNumberDAO extends GlobalDAO<PhoneNumber> {

  protected PhoneNumberDAO() {
    super(PhoneNumber.class);
  }

  @Override
  public PhoneNumber add(PhoneNumber object) {
    Transaction tr = getSession().beginTransaction();
    tr.begin();
    if (object.getContact() != null) {
      getSession().saveOrUpdate(object.getContact());
    }
    getSession().save(object);
    tr.commit();
    return object;

  }

}
