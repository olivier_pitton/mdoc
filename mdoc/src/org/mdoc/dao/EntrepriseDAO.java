package org.mdoc.dao;

import org.hibernate.Transaction;
import org.mdoc.domain.Entreprise;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;

public class EntrepriseDAO extends GlobalDAO<Entreprise> {

  EntrepriseDAO() {
    super(Entreprise.class);
  }

  @Override
  public Entreprise add(Entreprise object) {
    Transaction tr = getSession().beginTransaction();
    tr.begin();
    if (object.getAdd() != null) {
      getSession().saveOrUpdate(object.getAdd());
    }
    if (object.getGroups() != null) {
      for (GroupContact group : object.getGroups()) {
        getSession().saveOrUpdate(group);
      }
    }
    if (object.getProfiles() != null) {
      for (PhoneNumber n : object.getProfiles()) {
        getSession().saveOrUpdate(n);
      }
    }
    getSession().save(object);
    tr.commit();
    return object;
  }
}
