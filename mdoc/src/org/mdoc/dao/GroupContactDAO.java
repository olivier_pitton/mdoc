package org.mdoc.dao;

import org.hibernate.Transaction;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;

public class GroupContactDAO extends GlobalDAO<GroupContact> {

  protected GroupContactDAO() {
    super(GroupContact.class);
  }

  @Override
  public GroupContact add(GroupContact object) {
    Transaction tr = getSession().beginTransaction();
    tr.begin();
    if (object.getContact() != null) {
      for (Contact c : object.getContact()) {
        getSession().saveOrUpdate(c);
      }
    }
    getSession().save(object);
    tr.commit();
    return object;
  }
}
