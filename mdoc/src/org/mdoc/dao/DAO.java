package org.mdoc.dao;

import java.io.Closeable;
import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.mdoc.finder.Finder;
import org.mdoc.util.IWithName;

public interface DAO<T extends Serializable & IWithName> extends Closeable {

  T add(T object);

  void remove(T object);
  
  T merge(T object);

  T find(int id);

  List<T> findAll();

  @Override
  void close();

  Class<T> getObjectClass();
  
  void setSessionFactory(SessionFactory factory);
  
  void setFinder(Finder finder);

}
