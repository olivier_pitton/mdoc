package org.mdoc.util;

import java.io.Serializable;

public interface IFormattable extends IWithName, Serializable {

	String format();
	
}
