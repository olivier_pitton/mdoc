package org.mdoc.util;

public interface IExtensionnable {

	String getType();

	String getExtension();

}
