package org.mdoc.util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.jboss.logging.Logger;
import org.mdoc.dao.ContactDAO;
import org.mdoc.dao.DAO;
import org.mdoc.dao.DAOFactory;
import org.mdoc.domain.Contact;
import org.mdoc.jsf.bean.GroupBean;



@FacesConverter("contactConverter")
@SessionScoped
public class ContactConverter implements Converter{
	private static Logger logger = Logger.getLogger(GroupBean.class.getName());

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) throws ConverterException {
		//PickList p = (PickList) uiComponent;
		//DualListModel<Contact> contacts = (DualListModel<Contact>) p.getValue();
		//return contacts.getSource().get(Integer.parseInt(value));
		Contact c = null;
		try {
			DAOFactory dao = new DAOFactory();
			DAO<Contact> contactdao = dao.createContact();
			c = contactdao.find(Integer.parseInt(value));
			logger.info("--------- contact_ID = "+c.getId());
	    } catch (ClassCastException ce) {
	        FacesMessage errMsg = new FacesMessage("hum");
	        FacesContext.getCurrentInstance().addMessage(null, errMsg);
	        throw new ConverterException(errMsg.getSummary());
	    }
		return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) throws ConverterException {
		DAOFactory dao = new DAOFactory();
		DAO<Contact> contactdao = dao.createContact();
		return String.valueOf(((Contact) value).getId());
		
//		PickList p = (PickList) uiComponent;
//		DualListModel<Contact> contacts = (DualListModel<Contact>) p.getValue();
//		return String.valueOf(contacts.getSource().indexOf(value));
	}


}
