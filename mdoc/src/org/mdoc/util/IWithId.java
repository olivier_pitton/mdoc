package org.mdoc.util;

public interface IWithId {

  int getId();
  
  void setId(int id);
  
}

