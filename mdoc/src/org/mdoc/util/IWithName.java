package org.mdoc.util;

public interface IWithName extends IWithId {

	String getName();
	
	long getVersion();
	
	void setVersion(long version);
	
}
