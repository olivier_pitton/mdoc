package org.mdoc.jsf.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.mdoc.dao.DAO;
import org.mdoc.dao.DAOFactory;
import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Entreprise;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;
import org.mdoc.util.IFormattable;
import org.springframework.context.ApplicationContext;

public enum EnumCommand {

	LS("ls") {

		private String execute(Class<?> valClazz, ApplicationContext context) {
			DAOFactory factory = context.getBean(DAOFactory.class);
			DAO<? extends IFormattable> create = factory.create(valClazz, context);
			List<? extends IFormattable> findAll = create.findAll();
			if (findAll.isEmpty()) {
				return "";
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < findAll.size() - 1; i++) {
				sb.append(findAll.get(i).format()).append("<br />");
			}
			sb.append(findAll.get(findAll.size() - 1).format());
			return sb.toString();
		}

		private String handleHomeLs() {
			StringBuilder sb = new StringBuilder();
			Set<String> keySet = clazz.keySet();
			int size = keySet.size();
			int i = 0;
			for (String val : keySet) {
				sb.append(val);
				if (i < size - 1) {
					sb.append("<br />");
				}
				i++;
			}
			return sb.toString();
		}

		@Override
		public String handle(String[] parameters, String context, TerminalBean bean) {
			// Le cas où le user tape ls depuis le home, il voit
			// "contact, group ..."
			if (context == null) {
				if (parameters == null || parameters.length == 0) {
					return handleHomeLs();
				}
				// Il a fait ls contact, on lui affiche tous les contacts.
				Class<?> val = clazz.get(parameters[0]);
				// Si pas trouvé, il a tapé "ls" puis nimp "ls salut".
				if (val == null) {
					return MAN.handle(new String[] { toString() }, context, bean);
				}
				// On lui renvoie la requête
				return execute(val, bean.getContext());
			}
			Class<?> val = clazz.get(context);
			if (val == null) {
				return MAN.handle(new String[] { toString() }, context, bean);
			}
			return execute(val, bean.getContext());
		}
	},
	PWD("pwd") {
		@Override
		public String handle(String[] parameters, String context, TerminalBean bean) {
			if (context == null) {
				return "~";
			}
			return "~/" + context;
		}
	},
	RM("rm") {
		@SuppressWarnings("unchecked")
		@Override
		public String handle(String[] parameters, String context, TerminalBean bean) {
			if (parameters == null || parameters.length == 0 || context == null) {
				return MAN.handle(new String[] { toString() }, context, bean);
			}
			int id = -1;
			try {
				id = Integer.parseInt(parameters[0]);
			} catch (NumberFormatException e) {
				return MAN.handle(new String[] { toString() }, context, bean);
			}
			DAOFactory daoFactory = bean.getContext().getBean(DAOFactory.class);
			Class<? extends IFormattable> domainClazz = clazz.get(context);
			try (DAO<IFormattable> val = (DAO<IFormattable>) daoFactory.create(domainClazz, bean.getContext())) {
				IFormattable find = val.find(id);
				ResourceBundle bundle = ResourceBundle.getBundle("org.mdoc.jsf.i18n.messages", bean.getLocale());
				if (find == null) {
					return bundle.getString("rm_not_found_id");
				}
				val.remove(find);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("", String.format(bundle.getString("rm_success"), id)));
				return "";
			}
		}
	},
	CD("cd") {
		@Override
		public String handle(String[] parameters, String context, TerminalBean bean) {
			if (parameters == null || parameters.length == 0) {
				return MAN.handle(new String[] { toString() }, context, bean);
			}
			String param = parameters[0];
			if (param.equals("..") || param.equals("~")) {
				bean.setPath(null);
				return "";
			}
			for (String dir : clazz.keySet()) {
				if (dir.equals(param)) {
					bean.setPath(param);
					return "";
				}
			}
			return MAN.handle(new String[] { toString() }, context, bean);
		}
	},
	MAN("man") {
		@Override
		public String handle(String[] parameters, String context, TerminalBean bean) {
			ResourceBundle bundle = ResourceBundle.getBundle("org.mdoc.jsf.i18n.messages", bean.getLocale());
			if (parameters == null || parameters.length == 0) {
				return String.format(bundle.getString("man_man"), getAllCmds());
			}
			for (EnumCommand cmd : values()) {
				String string = cmd.toString();
				if (parameters[0].equals(string)) {
					return bundle.getString("man_" + string);
				}
			}
			return String.format(bundle.getString("man_man"), getAllCmds());
		}
	};

	final private String command;
	static private final Map<String, Class<? extends IFormattable>> clazz = init();

	static private final Map<String, Class<? extends IFormattable>> init() {
		Map<String, Class<? extends IFormattable>> res = new HashMap<>(5);
		res.put("contact", Contact.class);
		res.put("entreprise", Entreprise.class);
		res.put("group", GroupContact.class);
		res.put("address", Address.class);
		res.put("phone", PhoneNumber.class);
		return res;
	}

	private EnumCommand(String cmd) {
		command = cmd;
	}

	@Override
	public String toString() {
		return command;
	}

	public static String getAllCmds() {
		StringBuilder sb = new StringBuilder();
		EnumCommand[] values = values();
		for (int i = 0; i < values.length - 1; i++) {
			sb.append(values[i]).append(", ");
		}
		sb.append(values[values.length - 1]);
		return sb.toString();
	}

	abstract public String handle(String[] parameters, String context, TerminalBean bean);

	static public EnumCommand find(String command) {
		if (command == null) {
			return MAN;
		}
		for (EnumCommand cmd : values()) {
			if (cmd.command.equals(command)) {
				return cmd;
			}
		}
		return MAN;
	}
}
