package org.mdoc.jsf.bean;

import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "terminalBean")
@SessionScoped
public class TerminalBean extends SpringBean {

	private String path;

	public String handleCommand(String command, String[] params) {
		try {
			EnumCommand find = EnumCommand.find(command);
			return find.handle(params, path, this);
		} catch (Exception e) {
			e.printStackTrace();
			return EnumCommand.MAN.handle(params, path, this);
		}
	}

	protected Locale getLocale() {
		return FacesContext.getCurrentInstance().getViewRoot().getLocale();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
