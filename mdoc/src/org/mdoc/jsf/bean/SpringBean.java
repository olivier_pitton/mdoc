package org.mdoc.jsf.bean;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SpringBean implements Serializable {

	private ApplicationContext context;

	public SpringBean() {
	}

	protected final ApplicationContext getContext() {
		if (context == null) {
			ServletContext servletCcontext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			this.context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletCcontext);
		}
		return context;
	}

}
