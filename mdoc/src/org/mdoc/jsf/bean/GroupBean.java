package org.mdoc.jsf.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.mdoc.dao.DAO;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

@ManagedBean(name = "groupBean")
@SessionScoped
public class GroupBean extends SpringBean {

  /** Wizard debug **/
  private static Logger logger = Logger.getLogger(GroupBean.class.getName());

  /** GroupContact Selection **/
  protected List<GroupContact> groups;
  private GroupContact selectedGroups;
  private String groupName = "";

  /** Contacts PickList **/
  private DualListModel<Contact> contacts = new DualListModel<Contact>(new ArrayList<Contact>(), new ArrayList<Contact>());
  private List<Contact> contactsList = new ArrayList<Contact>();

  /**
   * @return the groupName
   */
  public String getGroupName() {
    return groupName;
  }

  /**
   * @param groupName
   *          the groupName to set
   */
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  /**
   * @return the contactsList
   */
  public List<Contact> getContactsList() {
    try (@SuppressWarnings("unchecked")
    DAO<Contact> cdao = (DAO<Contact>) getContext().getBean("contactDao")) {
      contactsList = cdao.findAll();
    }
    return contactsList;
  }

  /**
   * @param contactsList
   *          the contactsList to set
   */
  public void setContactsList(List<Contact> contactsList) {
    this.contactsList = contactsList;
  }

  @SuppressWarnings("unchecked")
  public GroupBean() {

    // Contacts
    List<Contact> csource = new ArrayList<Contact>();
    List<Contact> ctarget = new ArrayList<Contact>();
    try (DAO<Contact> cdao = (DAO<Contact>) getContext().getBean("contactDao")) {
      csource = cdao.findAll();
    }
    contacts = new DualListModel<Contact>(csource, ctarget);

  }

  public void onTransfer(TransferEvent event) {
    StringBuilder builder = new StringBuilder();
    for (Object item : event.getItems()) {
      builder.append(((Contact) item).getName()).append("<br />");
    }

    FacesMessage msg = new FacesMessage();
    msg.setSeverity(FacesMessage.SEVERITY_INFO);
    msg.setSummary("Items Transferred");
    msg.setDetail(builder.toString());

    FacesContext.getCurrentInstance().addMessage(null, msg);
  }

  /**
   * Create a group of contacts in the database
   * 
   * @param event
   */
  @SuppressWarnings("unchecked")
  public void createGroup(ActionEvent event) {
    GroupContact group = new GroupContact();
    group.setGroupName(groupName);
    try (DAO<GroupContact> gdao = (DAO<GroupContact>) (getContext().getBean("groupContactDao"))) {
      gdao.add(group);
    }
    groupName = new String();
    getGroups();
  }

  /**
   * @return the contacts
   */
  public DualListModel<Contact> getContacts() {
    List<Contact> csource = getContactsList();
    List<Contact> ctarget = new ArrayList<Contact>(selectedGroups.getContact());
    List<Contact> toDelete = new ArrayList<Contact>();

    // Suppression des contacts
    if ((!csource.isEmpty()) & (!ctarget.isEmpty())) {
      for (Contact c1 : csource) {
        for (Contact c2 : ctarget) {
          if (c1.getId() == c2.getId()) {
            toDelete.add(c1);
          }
        }
      }
      csource.removeAll(toDelete);
    }

    contacts = new DualListModel<Contact>(csource, ctarget);
    return contacts;
  }

  /**
   * @param contacts
   *          the contacts to set
   */
  public void setContacts(DualListModel<Contact> contacts) {
    this.contacts = contacts;
  }

  public List<GroupContact> getGroups() {
    try (@SuppressWarnings("unchecked")
    DAO<GroupContact> dao = (DAO<GroupContact>) getContext().getBean("groupContactDao")) {
      groups = new ArrayList<GroupContact>(dao.findAll());
      return groups;
    }
  }

  public void setGroups(List<GroupContact> groups) {
    this.groups = groups;
  }

  /**
   * save the contacts
   */
  public void saveContacts() {
    List<Object> ctmp = new ArrayList<Object>(contacts.getTarget());
    List<Contact> contacts = new ArrayList<Contact>();
    try (@SuppressWarnings("unchecked")
    DAO<GroupContact> gdao = (DAO<GroupContact>) getContext().getBean("groupContactDao")) {
      int id;
      for (int i = 0; i < ctmp.size(); i++) {
        id = Integer.parseInt((String) ctmp.get(i));
        Contact c = searchContact(id);
        contacts.add(c);
//        if (!((selectedGroups.getContact()).contains(c))) {
//          (selectedGroups.getContact()).add(c);
//        }
        selectedGroups.setContact(contacts);
        selectedGroups = gdao.merge(selectedGroups);
      }

    }
  }

  private Contact searchContact(int id) {
    Contact c = null;
    for (Contact ctmp : getContactsList()) {
      if (id == ctmp.getId()) {
        return ctmp;
      }
    }
    return c;
  }

  public void remove() {
    try (@SuppressWarnings("unchecked")
    DAO<GroupContact> gdao = (DAO<GroupContact>) getContext().getBean("groupContactDao")) {
      for (Contact c : selectedGroups.getContact()) {
        c.getGroups().remove(selectedGroups);
      }

      gdao.remove(selectedGroups);
      groups.remove(selectedGroups);
      selectedGroups = groups.get(0);
    }
  }

  /**
   * Handle Wizard next step
   * 
   * @param event
   * @return
   */
  public String onFlowProcess(FlowEvent event) {
    logger.info("Current wizard step:" + event.getOldStep());
    logger.info("Next step:" + event.getNewStep());
    return event.getNewStep();
  }

  /**
   * @return the selectedGroups
   */
  public GroupContact getSelectedGroups() {
    return selectedGroups;
  }

  /**
   * @param selectedGroups
   *          the selectedGroups to set
   */
  public void setSelectedGroups(GroupContact selectedGroups) {
    this.selectedGroups = selectedGroups;
  }
}
