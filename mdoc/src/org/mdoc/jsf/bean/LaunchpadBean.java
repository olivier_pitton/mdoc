package org.mdoc.jsf.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;

@ManagedBean(name = "launchpadBean")
@RequestScoped
public class LaunchpadBean implements Serializable {

	private DashboardModel launchpad;

	public DashboardModel getLaunchpad() {
		return launchpad;
	}

	public void setLaunchpad(DashboardModel launchpad) {
		this.launchpad = launchpad;
	}

	@PostConstruct
	private void init() {
		launchpad = new DefaultDashboardModel();
		addColumn("dash-finder");
		addColumn("dash-contacts");
		addColumn("dash-relation");
		addColumn("dash-terminal");
		addColumn("dash-theme");
		addColumn("dash-exporter");
	}

	private void addColumn(String id) {
		DashboardColumn column = new DefaultDashboardColumn();
		column.addWidget(id);
		launchpad.addColumn(column);
	}

}
