package org.mdoc.jsf.bean;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.mdoc.dao.DAO;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.mindmap.DefaultMindmapNode;
import org.primefaces.model.mindmap.MindmapNode;


@ManagedBean(name = "relationBean")
@ViewScoped
public class RelationBean extends SpringBean {

	private MindmapNode root, selected;

	public RelationBean() {
		init();
	}

	public void refresh() {
		init();
	}
	
	private void init() {		
		root = new DefaultMindmapNode("Racine", "", "FFCC00", false);
		selected = null;
		Set<GroupContact> cache = new HashSet<>();
		try(@SuppressWarnings("unchecked")
		DAO<GroupContact> dao = (DAO<GroupContact>) getContext().getBean("groupContactDao")) {
			List<GroupContact> findAll = dao.findAll();
			if(findAll.isEmpty()) {
				return;
			}
			for(GroupContact gc : findAll) {
			  addGroup(gc, cache);
			}
		}
	}

	private void addGroup(GroupContact gc, Set<GroupContact> cache) {
	  	if(cache.contains(gc)) {
			return;
		}
		if(gc == null) {
		  return;
		}
		DefaultMindmapNode groupNode = new DefaultMindmapNode(gc.getGroupName(), "", "6e9ebf", true);
		cache.add(gc);
		root.addNode(groupNode);
		List<Contact> contacts = gc.getContact();
		if(contacts == null || contacts.isEmpty()) {
			return;
		}
		for(Contact c : gc.getContact()) {
			if(c == null) {
				continue;
			}
			DefaultMindmapNode contactNode = new DefaultMindmapNode(c.getLastName(), "", "82c542", true);
			groupNode.addNode(contactNode);
			List<GroupContact> groups = c.getGroups();
			if(groups == null || groups.isEmpty()) {
				continue;
			}
			for(GroupContact child : groups) {
			  if(!cache.contains(gc)) {
			    addGroup(child, cache);
			  }
			}
		}
	}

	public MindmapNode getRoot() {
		return root;
	}

	public void setRoot(MindmapNode root) {
		this.root = root;
	}

	public MindmapNode getSelected() {
		return selected;
	}

	public void setSelected(MindmapNode selected) {
		this.selected = selected;
	}

	public void onNodeSelect(SelectEvent event) {  
        this.selected = (MindmapNode) event.getObject();  
    }
}
