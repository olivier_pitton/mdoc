package org.mdoc.jsf.bean;

import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.mdoc.dao.ContactDAO;
import org.mdoc.dao.DAO;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Contacts;
import org.mdoc.domain.GroupContact;
import org.mdoc.social.DataProvider;
import org.mdoc.social.LinkedInDataProvider;
import org.primefaces.event.FlowEvent;

@ManagedBean(name = "socialBean")
@ViewScoped
public class SocialBean extends SpringBean {

	private DataProvider provider;
	private int type;
	private String verifier;
	private String url;

	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}

	public String getVerifier() {
		return verifier;
	}

	public void setVerifier(String verifier) {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle bundle = ResourceBundle.getBundle("org.mdoc.jsf.i18n.messages", context.getViewRoot().getLocale());
		try {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Début du téléchargement des contacts..."));
			provider.setVerifier(verifier);
			Contacts contacts2 = provider.getContacts(getContext());
			if(contacts2 == null) {
				String message = bundle.getString("social_verifier_error");
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", message));				
				return;
			}
			GroupContact group = new GroupContact("LinkedIn");
			try(DAO<GroupContact> gc = (DAO<GroupContact>) getContext().getBean("groupContactDao")) {
				gc.add(group);
			}
			List<Contact> contacts = contacts2.getContacts();
			group.setContact(contacts);
			try(DAO<Contact> dao = (DAO<Contact>) getContext().getBean("contactDao")) {
				for(Contact c : contacts) {
					dao.add(c);
				}
			}
			try(DAO<GroupContact> gc = (DAO<GroupContact>) getContext().getBean("groupContactDao")) {
				gc.merge(group);
			}
			String message = String.format(bundle.getString("social_verifier_success"), contacts.size());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		provider = getContext().getBean("linkedinDataProvider", LinkedInDataProvider.class).build("proxy", 3128);
		this.url = provider.getAuthorizationUrl();
	}

	public void setUrl(String u) {
		url = u;
	}

	public String getUrl() {
		return this.url;
	}
}