package org.mdoc.jsf.bean;


import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.mdoc.dao.DAO;
import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.PhoneNumber;
import org.primefaces.event.FlowEvent;

//import com.sun.istack.internal.logging.Logger;

@ManagedBean(name = "contactBean")
@SessionScoped
public class ContactBean extends SpringBean {

  protected List<Contact> contacts;
  protected Contact[] selectedContacts;
  private Contact contact = new Contact();
  private Address add = new Address();
  private List<PhoneNumber> profiles = new ArrayList<PhoneNumber>();
  private PhoneNumber dom = new PhoneNumber();
  private PhoneNumber pro = new PhoneNumber();
  private PhoneNumber mob = new PhoneNumber();
  
  //private static Logger logger = Logger.getLogger(ContactBean.class.getName());
  
  public ContactBean() {
    contact.setAdd(add);
    
    dom.setPhoneKind("Domicile");
    pro.setPhoneKind("Pro");
    mob.setPhoneKind("Mobile");
    
    profiles.add(dom);
    profiles.add(mob);
    profiles.add(pro);
    
    contact.setProfiles(profiles);
    
    /*contact.getProfiles().add(dom);
    contact.getProfiles().add(mob);
    contact.getProfiles().add(pro);*/
  }
  
  public List<PhoneNumber> getProfiles(){
    return profiles;
  }
  
  public void setProfiles(List<PhoneNumber> profiles){
    this.profiles = profiles;
  }
  
  public Contact getContact(){
    return contact;
  }
  public void setContact(Contact contact){
   this.contact = contact;
  }
  
  public Contact[] getSelectedContacts() {
    return selectedContacts;
  }

  public void setSelectedContacts(Contact[] selectedContacts) {
    this.selectedContacts = selectedContacts;
  }

  @SuppressWarnings("unchecked")
  public List<Contact> getContacts() {
    try (DAO<Contact> dao = (DAO<Contact>) getContext().getBean("contactDao")) {
      contacts = dao.findAll();
      return contacts;
    }
  }

  public void setContacts(List<Contact> contacts) {
    this.contacts = contacts;
  }


  public void remove() {
    FacesContext context = FacesContext.getCurrentInstance();
    ResourceBundle bundle = ResourceBundle.getBundle("org.mdoc.jsf.i18n.messages", context.getViewRoot().getLocale());
    if (selectedContacts == null || selectedContacts.length == 0) {
      context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", bundle.getString("error_no_selected_rows")));
      return;
    }
    StringBuilder sb = new StringBuilder();
    sb.append(bundle.getString("deleted_contact")).append(" ");
    try (@SuppressWarnings("unchecked")
    DAO<Contact> dao = (DAO<Contact>) getContext().getBean("contactDao")) {
      for (int i = 0; i < selectedContacts.length; i++) {
        Contact co = selectedContacts[i];
        sb.append(co.getEmail());
        if (i < (selectedContacts.length - 1)) {
          sb.append(", ");
        }
        dao.remove(co);
        contacts.remove(co);
      }
    }
    context.addMessage(null, new FacesMessage("", sb.toString()));
  }
  
  @SuppressWarnings("unchecked")
  public void add() {
   FacesContext context = FacesContext.getCurrentInstance();
    ResourceBundle bundle = ResourceBundle
        .getBundle("org.mdoc.jsf.i18n.messages", context.getViewRoot()
            .getLocale());
    try (DAO<Contact> c = (DAO<Contact>) getContext().getBean("contactDao")) {
      c.add(contact);
      contacts.add(contact);
    }
    context.addMessage(null, new FacesMessage("", "Le contact " + contact.getLastName() + " a bien été ajouté."));
    this.contact = new Contact();
    this.contact.setAdd(new Address());
    dom.setPhoneKind("Domicile");
    pro.setPhoneKind("Pro");
    mob.setPhoneKind("Mobile");
    
    profiles.clear();
    profiles.add(dom);
    profiles.add(mob);
    profiles.add(pro);
    
    this.contact.setProfiles(profiles);
    
  }	 
}
