package org.mdoc.jsf.bean;

import java.io.ByteArrayInputStream;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.mdoc.dao.DAO;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Contacts;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.Groups;
import org.mdoc.exporter.Exporter;
import org.mdoc.exporter.JsonExporter;
import org.mdoc.exporter.XmlExporter;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.ApplicationContext;

@ManagedBean(name = "downloadBean")
@RequestScoped
public class DownloadBean extends SpringBean {

	private StreamedContent file;

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public void groups_json() {
		exportGroups(getContext().getBean(JsonExporter.class));
	}

	public void groups_xml() {
		exportGroups(getContext().getBean(XmlExporter.class));
	}

	public void contacts_json() {
		exportContacts(getContext().getBean(JsonExporter.class));
	}

	public void contacts_xml() {
		exportContacts(getContext().getBean(XmlExporter.class));
	}

	@SuppressWarnings("unchecked")
	private void exportGroups(Exporter exporter) {
		ApplicationContext context = getContext();
		try (DAO<GroupContact> dao = (DAO<GroupContact>) context.getBean("groupContactDao")) {
			Groups g = context.getBean(Groups.class);
			g.setGroups(dao.findAll());
			String res = exporter.exportData(g);
			ByteArrayInputStream bais = new ByteArrayInputStream(res.getBytes());
			setFile(new DefaultStreamedContent(bais, exporter.getType(), "groups." + exporter.getExtension()));
			bais.close();
		} catch (Exception e) {
			e.printStackTrace();
			setFile(null);
			FacesContext faces = FacesContext.getCurrentInstance();
			ResourceBundle bundle = ResourceBundle.getBundle("org.mdoc.jsf.i18n.messages", faces.getViewRoot().getLocale());
			faces.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", bundle.getString("error_download")));
		}
	}

	private void exportContacts(Exporter exporter) {
		ApplicationContext context = getContext();
		try (@SuppressWarnings("unchecked")
		DAO<Contact> dao = (DAO<Contact>) context.getBean("contactDao")) {
			Contacts c = context.getBean(Contacts.class);
			c.setContacts(dao.findAll());
			String res = exporter.exportData(c);
			ByteArrayInputStream bais = new ByteArrayInputStream(res.getBytes());
			setFile(new DefaultStreamedContent(bais, exporter.getType(), "contacts." + exporter.getExtension()));
			bais.close();
		} catch (Exception e) {
			e.printStackTrace();
			setFile(null);
			FacesContext faces = FacesContext.getCurrentInstance();
			ResourceBundle bundle = ResourceBundle.getBundle("org.mdoc.jsf.i18n.messages", faces.getViewRoot().getLocale());
			faces.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", bundle.getString("error_download")));

		}
	}

}
