package org.mdoc.jsf.bean;

import java.util.List;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.mdoc.finder.Finder;
import org.mdoc.finder.LuceneFinder;
import org.mdoc.finder.SearchResult;

@ManagedBean(name = "finderBean")
@SessionScoped
public class FinderBean extends SpringBean {

	private String searchRequest;
	private List<SearchResult> datas = new ArrayList<>();;

	public void executeSearch() {
		Finder finder = getContext().getBean("finder", LuceneFinder.class);
		try {
			System.out.println("La recherche est " + searchRequest);
			if (searchRequest == null || searchRequest.trim().isEmpty()) {
				setDatas(finder.findAll());
			} else {
				setDatas(finder.find(searchRequest, 50));
			}
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = ResourceBundle.getBundle("org.mdoc.jsf.i18n.messages", context.getViewRoot().getLocale());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", String.format(bundle.getString("invalid_finder_request"), searchRequest)));
		}
	}

	public String getSearchRequest() {
		return searchRequest;
	}

	public void setSearchRequest(String searchRequest) {
		this.searchRequest = searchRequest;
	}

	public List<SearchResult> getDatas() {
		return datas;
	}

	public void setDatas(List<SearchResult> datas) {
		this.datas = datas;
	}

}
