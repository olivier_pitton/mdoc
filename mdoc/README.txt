__________________________________________________________________
__	NI404 Middlewares orientés composants       				__
__________________________________________________________________				
__	Projet : MDOC OSX											__
__		Wandrille Domin, Olivier Pitton, Marie-Diana Tran		__
__________________________________________________________________


------------------------------------------------------------------
1 - Frameworks
------------------------------------------------------------------

Java : 					JDK 1.7.0_45

Hibernate :				Hibernate 3

Spring :				Spring 4.0

Serveur :				Jboss 7.1.1 Brontes

SGBD :					H2 1.3.173 
Il s'agit d'une base de données embarquée.
Elle ne requiert aucune installation ou configuration en sus.
	
Primefaces :			Primefaces 4.0

Moteur de recherche : 	Apache Lucene 4.6.0

Bibliothèques :			Scribe 1.3.5
						Jackson 2.2.3 (Gestion Json)



----------------------------------------------------------------
2 - Installation
----------------------------------------------------------------
REQUIREMENTS : 
/a\ Définir une variable d'environnement JBOSS_HOME
	qui pointe sur le répertoire d'installation de Jboss
	
	
2.1 Importer le projet dans Eclipse.

2.2 Décompresser le dossier compressé lib.tar
	dans le dossier ${mon_workspace}/mdoc/war/WEB-INF/lib.
	
2.3 Déploiement

2.3.1 Serveur local

2.3.1.1 Générer le fichier mdoc.war avec le fichier ant build.xml
			a- en ligne de commande en tapant dans la console :
				>ant compile
				>ant war
	
			b- avec eclipse
				Faire un clic droit sur le fichier build.xml dans
				l'arborescence du projet.
				Sélectionner Run as puis choisir Ant build.
				Sélectionner dans la fenetre, au moins les targets :
				compile et war

2.3.1.2 Déployer le fichier mdoc.war obtenu dans le serveur JBOSS
		Cela peut se faire de plusieurs manières :
	
			a- avec le fichier ant build.xml avec la cible deploy
			soit en ligne de commande en tapant dans la console
				>ant deploy-jboss
			soit avec eclipse en cliquant sur le fichier build.xml
			et en sélectionnant le target deploy-jboss après avoir
			choisi ant build.
		
			b- avec l'interface d'eclipse :
			Sélectionner la racine du projet dans l'explorateur
			de projets Eclipse.
			Cliquer sur Run as puis sur Run on Server.
			Choisir enfin le serveur JBOSS 7.1 dans la fenetre.
			Si celui-ci n'est pas dans la liste proposée,
			cliquer sur Manually define a new server.
			Puis
				soit choisir de pointer sur le répertoire principal
				de l'installation du serveur JBoss,
				soit choisir de le télécharger et l'installer via
				Eclipse.
			

--------------------------------------------------------------------
3 - Jeux de tests
--------------------------------------------------------------------
