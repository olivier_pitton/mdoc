package org.mdoc.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.mdoc.domain.Entreprise;

public class EntrepriseDAOTest extends GlobalDAOTest<Entreprise> {

    public EntrepriseDAOTest() {
    super(factory.createEntreprise());
  }

  @Override
  protected final Entreprise create(String name) {
    Entreprise entreprise = new Entreprise();
    entreprise.setNumSiret(10);
    return entreprise;
  }

  @Override
  protected void after(Entreprise object) {
    assertNotNull(object);
    assertEquals(10, object.getNumSiret());
  }

	@Override
	protected void update(Entreprise object) {
		object.setNumSiret((int) (Math.random() * 10000)); 
	}

}
