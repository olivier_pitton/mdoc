package org.mdoc.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;

public class GroupContactDAOTest extends GlobalDAOTest<GroupContact> {

	private Contact contact;

	public GroupContactDAOTest() {
		super(factory.createGroupContact());
	}

	@Override
	protected GroupContact create(String name) {
		GroupContact g = new GroupContact(name, new ArrayList<Contact>());
		contact = new Contact(name, name, name);
		g.getContact().add(contact);
		return g;
	}

	@Override
	protected void after(GroupContact object) {
		assertNotNull(object.getContact());
		assertEquals(contact, object.getContact().iterator().next());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void multiInsert() {
		System.out.println("Chargement des données en base...");
		List<Contact> contacts = new ArrayList<>();
		List<GroupContact> groups = new ArrayList<>();
		try (DAO<Contact> dao = (DAO<Contact>) context.getBean("contactDao")) {
			dao.add(new Contact("Wandrille", "Domin", "Sonmail@gmail.com"));
			dao.add(new Contact("Olivier", "Pitton", "olivier.pitton@gmail.com"));
			dao.add(new Contact("Jean", "Ly", "jean.ly@gmail.com"));
			contacts.addAll(dao.findAll());
			System.out.println(contacts);
		}
		try (DAO<GroupContact> dao = (DAO<GroupContact>) context.getBean("groupContactDao")) {
			GroupContact first = new GroupContact("MDOC");
			dao.add(first);
			GroupContact second = new GroupContact("Default");
			dao.add(second);
			groups.addAll(dao.findAll());
			assertEquals(first, groups.get(0));
			assertEquals(second, groups.get(1));
		}
		try (DAO<GroupContact> dao = (DAO<GroupContact>) context.getBean("groupContactDao")) {
			groups.get(0).setContact(contacts);
			groups.get(1).setContact(
					Arrays.asList(contacts.get(0), contacts.get(1)));
			dao.add(groups.get(0));
			dao.add(groups.get(1));
		}
		groups = ((DAO<GroupContact>) context.getBean("groupContactDao")).findAll();
		System.out.println(groups);
		for (GroupContact gc : groups)
			System.out.println(gc.getContact());
		
		try (DAO<Contact> dao = (DAO<Contact>) context.getBean("contactDao")) {
			for(Contact c : dao.findAll()) {
				System.out.println(c.getGroups());
			}
		}
	}

	@Override
	protected void update(GroupContact object) {
		object.setGroupName(Math.random()  + "");
	}
}
