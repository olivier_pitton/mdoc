package org.mdoc.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;

public class ContactDAOTest extends GlobalDAOTest<Contact> {

	private List<GroupContact> gc;
	private Address address;

	public ContactDAOTest() {
		super(factory.createContact());
	}

	@Override
	protected Contact create(String name) {
		Contact c = new Contact(name, name, name);
		address = new Address(name, name, name, name);
		c.setAdd(address);
		gc = Arrays.asList(new GroupContact(name, null));
		c.setGroups(gc);
		return c;
	}

	@Override
	protected void after(Contact object) {
		assertNotNull(object);

		assertEquals(address, object.getAdd());
		assertTrue(address.getId() > 0);

		assertNotNull(object.getGroups());
		assertEquals(1, object.getGroups().size());
		assertEquals(object.getGroups().iterator().next(), gc.get(0));
		assertTrue(gc.get(0).getId() > 0);
	}

	@Override
	protected void update(Contact object) {
		object.setEmail(Math.random() + "");
	}

}
