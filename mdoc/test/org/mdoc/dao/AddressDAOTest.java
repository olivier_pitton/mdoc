package org.mdoc.dao;

import org.mdoc.domain.Address;

public class AddressDAOTest extends GlobalDAOTest<Address> {

	public AddressDAOTest() {
		super(factory.createAddress());
	}

	@Override
	protected final Address create(String val) {
		return new Address(val, val, val, val);
	}

	@Override
	protected void update(Address object) {
		object.setCity(Math.random() + "");
	}
}
