package org.mdoc.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.mdoc.domain.Contact;
import org.mdoc.domain.PhoneNumber;

public class PhoneNumberDAOTest extends GlobalDAOTest<PhoneNumber> {

  private Contact contact;
  
  public PhoneNumberDAOTest() {
    super(factory.createPhoneNumber());
  }

  @Override
  protected PhoneNumber create(String name) {
    PhoneNumber phoneNumber = new PhoneNumber(name, name);
    contact = new Contact(name, name, name);
    phoneNumber.setContact(contact);
    return phoneNumber;
  }
  
  @Override
  protected void after(PhoneNumber object) {
    assertNotNull(object.getContact());
    assertEquals(contact, object.getContact());
  }

	@Override
	protected void update(PhoneNumber object) {
		object.setPhoneNumber(Math.random() + "");		
	}
}
