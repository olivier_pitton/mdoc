package org.mdoc.dao;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mdoc.domain.Contact;
import org.mdoc.domain.GroupContact;
import org.mdoc.finder.LuceneFinder;
import org.mdoc.util.IWithName;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public abstract class GlobalDAOTest<T extends Serializable & IWithName> {

	static protected final DAOFactory factory = new DAOFactory();

	@BeforeClass
	static public void beforeClass() {
		context = new FileSystemXmlApplicationContext("war/WEB-INF/applicationContext.xml");
	}

	@AfterClass
	static public void afterClass() {
		context.close();
	}

	static FileSystemXmlApplicationContext context;

	final protected DAO<T> dao;

	public GlobalDAOTest(DAO<T> dao) {
		this.dao = dao;
		this.dao.setSessionFactory((SessionFactory) context.getBean("sessionFactory"));
		this.dao.setFinder(context.getBean("finder", LuceneFinder.class));
	}

	@SuppressWarnings("unchecked")
	@After
	public void clean() {
		DAO<Contact> c = (DAO<Contact>) context.getBean("contactDao");
		assertNotNull(c);
		for (Contact contact : c.findAll()) {
		  c.remove(contact);
		}
		assertEquals(0, c.findAll().size());
		DAO<GroupContact> gc = (DAO<GroupContact>) context.getBean("groupContactDao");
		System.out.println("Salut 4");
		for (GroupContact g : gc.findAll()) {
			gc.remove(g);
		}
		System.out.println("Salut 5");
		assertEquals(0, gc.findAll().size());
	}

	protected void after(T object) {

	}

	@Test(timeout = 3000L)
	public final void testAdd() {
		T create = create("bonjour");
		assertEquals(create, dao.add(create));
		assertTrue(create.getId() >= 0);
		T find = dao.find(create.getId());
		assertEquals(find, create);
		after(create);
		dao.remove(find);
	}

	@Test(timeout = 3000L)
	public final void testRemove() {
		T create = create("bonjour !");
		assertEquals(create, dao.add(create));
		assertTrue(create.getId() >= 0);
		int id = create.getId();
		dao.remove(create);
		assertEquals(dao.find(id), null);
		after(create);
	}

	@Test(timeout = 3000L)
	public final void testFindAll() {
		T create = create("falutfava");
		dao.add(create);

		T create2 = create("falutfava2");
		dao.add(create2);

		List<T> res = dao.findAll();
		assertNotNull(res);
		assertEquals(2, res.size());
		assertEquals(create, res.get(0));
		assertEquals(create2, res.get(1));
		after(create2);
		dao.remove(create);
		dao.remove(create2);
	}

	@Test(timeout = 3000L)
	public void testMerge() {
		T create = create("yop");
		dao.add(create);
		dao.merge(create);
		update(create);
		create = dao.merge(create);
		assertEquals(1L, create.getVersion());
		T first = dao.find(create.getId());
		assertEquals(first, create);
		assertEquals(first.getVersion(), create.getVersion());
		after(first);
		dao.remove(first);
	}

	@Test(timeout = 3000L)
	public void testMergeWithError() {
		T create = create("yop2");
		dao.add(create);
		update(create);
		T first = dao.find(create.getId());
		dao.merge(create);
		assertSame(create, first);
		assertEquals(1L, create.getVersion());
		after(create);
		dao.remove(create);
	}

	abstract protected T create(String name);

	abstract protected void update(T object);
}
