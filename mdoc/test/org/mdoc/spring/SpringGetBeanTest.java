package org.mdoc.spring;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mdoc.dao.ContactDAO;
import org.mdoc.dao.EntrepriseDAO;
import org.mdoc.dao.GroupContactDAO;
import org.mdoc.dao.PhoneNumberDAO;
import org.mdoc.domain.Address;
import org.mdoc.domain.Contacts;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.Groups;
import org.mdoc.domain.PhoneNumber;
import org.mdoc.exporter.ExporterFactory;
import org.mdoc.exporter.JsonExporter;
import org.mdoc.exporter.XmlExporter;
import org.mdoc.finder.LuceneFinder;
import org.mdoc.importer.ImporterFactory;
import org.mdoc.importer.JsonImporter;
import org.mdoc.importer.LinkedInImporter;
import org.mdoc.importer.XmlImporter;
import org.mdoc.social.DataProviderFactory;
import org.mdoc.social.LinkedInDataProvider;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@RunWith(Parameterized.class)
public class SpringGetBeanTest {

	@Parameters
	static public Collection<Object[]> param() {
		List<Object[]> l = new ArrayList<>();
		l.add(new Object[] { "contact", null });
		l.add(new Object[] { "contacts", Contacts.class });
		l.add(new Object[] { "groups", Groups.class });
		l.add(new Object[] { "entreprise", null });
		l.add(new Object[] { "phoneNumber", PhoneNumber.class });
		l.add(new Object[] { "address" , Address.class});
		l.add(new Object[] { "groupContact", GroupContact.class });
		l.add(new Object[] { "jsonImporter", JsonImporter.class});
		l.add(new Object[] { "xmlImporter", XmlImporter.class });
		l.add(new Object[] { "linkedinImporter", LinkedInImporter.class });
		l.add(new Object[] { "xmlExporter", XmlExporter.class });
		l.add(new Object[] { "jsonExporter", JsonExporter.class });
		l.add(new Object[] { "importerFactory", ImporterFactory.class });
		l.add(new Object[] { "exporterFactory", ExporterFactory.class });
		l.add(new Object[] { "entrepriseDao", EntrepriseDAO.class });
		l.add(new Object[] { "contactDao", ContactDAO.class });
		l.add(new Object[] { "phoneNumberDao", PhoneNumberDAO.class });
		l.add(new Object[] { "addressDao", null });
		l.add(new Object[] { "groupContactDao", GroupContactDAO.class });
		l.add(new Object[] { "linkedinDataProvider", LinkedInDataProvider.class });
		l.add(new Object[] { "dataProviderFactory", DataProviderFactory.class });
		l.add(new Object[] { "finder", LuceneFinder.class });
		return l;
	}

	@BeforeClass
	static public void beforeClass() {
		context = new FileSystemXmlApplicationContext("war/WEB-INF/applicationContext.xml");
	}

	@AfterClass
	static public void afterClass() {
		context.close();
	}

	static private FileSystemXmlApplicationContext context;

	private final String beanName;
	private final Class<?> clazz;

	public SpringGetBeanTest(String name, Class<?> clazz) {
		beanName = name;
		this.clazz = clazz;
	}

	@Test
	public void testDomainBean() {
		assertNotNull(context.getBean(beanName));
		if(clazz != null) {
			assertNotNull(context.getBean(beanName, clazz));
			assertEquals(clazz, context.getBean(beanName, clazz).getClass());
		}
	}

}
