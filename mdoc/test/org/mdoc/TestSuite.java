package org.mdoc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.mdoc.dao.AddressDAOTest;
import org.mdoc.dao.ContactDAOTest;
import org.mdoc.dao.EntrepriseDAOTest;
import org.mdoc.dao.GroupContactDAOTest;
import org.mdoc.dao.PhoneNumberDAOTest;
import org.mdoc.export.GlobalExporterTest;
import org.mdoc.finder.LuceneFinderTest;
import org.mdoc.spring.SpringGetBeanTest;

@RunWith(Suite.class)
@SuiteClasses({
  AddressDAOTest.class,
  PhoneNumberDAOTest.class,
  ContactDAOTest.class,
  EntrepriseDAOTest.class,
  GroupContactDAOTest.class,
  GlobalExporterTest.class,
  SpringGetBeanTest.class,
  LuceneFinderTest.class
})
public class TestSuite {

}
