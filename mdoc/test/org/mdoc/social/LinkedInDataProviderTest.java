package org.mdoc.social;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mdoc.dao.DAO;
import org.mdoc.dao.DAOFactory;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Contacts;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class LinkedInDataProviderTest {

	@BeforeClass
	static public void beforeClass() {
		context = new FileSystemXmlApplicationContext("war/WEB-INF/applicationContext.xml");
	}

	@AfterClass
	static public void afterClass() {
		context.close();
	}

	static private FileSystemXmlApplicationContext context;

	
	//@Test
	public final void testGetReadContacts() throws Exception {
		try (FileInputStream fileInputStream = new FileInputStream(
				"data/linkedin.xml")) {
			byte[] b = new byte[fileInputStream.available()];
			fileInputStream.read(b);
			Contacts contacts = new DataProviderFactory().createLinkedIn()
					.getContacts(context, new String(b));
			try (DAO<Contact> dao = new DAOFactory().createContact()) {
				for (Contact c : contacts) {
					dao.add(c);
				}
				int size = contacts.size();
				contacts = new Contacts(dao.findAll());
				assertEquals(contacts.size(), size);
				for (Contact c : contacts) {
					dao.remove(c);
				}
			}

		}
	}

	@Test
	public final void testGetContacts() throws Exception {
		LinkedInDataProvider build = (LinkedInDataProvider) new DataProviderFactory().createLinkedIn().build("proxy", 3128);
		System.out.println(build.getAuthorizationUrl());
		try (Scanner scanner = new Scanner(System.in)) {
			build.setVerifier(scanner.nextLine());

			String res = build.send();
			try (FileOutputStream fos = new FileOutputStream(
					"data/linkedin.xml")) {
				fos.write(res.getBytes());
				fos.flush();
			}

		}

	}

}
