package org.mdoc.export;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mdoc.domain.Address;
import org.mdoc.domain.Contact;
import org.mdoc.domain.Contacts;
import org.mdoc.domain.GroupContact;
import org.mdoc.domain.PhoneNumber;
import org.mdoc.exporter.Exporter;
import org.mdoc.exporter.ExporterFactory;
import org.mdoc.importer.Importer;
import org.mdoc.importer.ImporterFactory;

@RunWith(Parameterized.class)
public class GlobalExporterTest {

	static private final ExporterFactory factory = new ExporterFactory();
	static private final ImporterFactory imp = new ImporterFactory();

	@Parameters
	static public List<Object[]> param() {
		List<Object[]> l = new ArrayList<Object[]>(2);
		l.add(new Object[] { factory.createXml(), imp.createXml() });
		l.add(new Object[] { factory.createJson() , imp.createJson()});
		return l;
	}

	private final Exporter exporter;
	private final Importer importer;
	
	public GlobalExporterTest(Exporter e, Importer i) {
		exporter = e;
		importer = i;
	}
	
	@Test
	public final void exportListOfObject() throws Exception {
		List<Contact> contacts = Arrays.asList(mock("first"), mock("second"),
				mock("third"));
		Contacts ob = new Contacts(contacts);
		String res = exporter.exportData(ob);
		Contacts data = (Contacts) importer.importData(res, Contacts.class);
		assertNotNull(data);
		assertEquals(ob, data);
	}

	private Contact mock(String name) {
		Contact c = new Contact(name, name, name);
		Address address = new Address(name, name, name, name);
		c.setAdd(address);
		List<GroupContact> gc = Arrays.asList(new GroupContact(name, null));
		c.setGroups(gc);
		gc.get(0).setContact(Arrays.asList(c));
		c.setProfiles(Arrays.asList(new PhoneNumber("+33", "0676564554")));
		return c;
	}
}
