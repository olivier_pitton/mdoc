package org.mdoc.finder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.Test;
import org.mdoc.domain.Contact;

public class LuceneFinderTest {

	@Test(timeout = 10000L)
	public void testFinder() throws IOException, ParseException {
		try (Finder finder = new LuceneFinder()) {
			finder.init();
			finder.index(create("olivier"));
			finder.index(create("wandrille"));
			finder.index(create("jean"));
			List<SearchResult> find = finder.find("name:*l*");
			assertNotNull(find);
			assertEquals(2, find.size());
			assertEquals("olivier", find.get(0).getName());
			assertEquals("wandrille", find.get(1).getName());
			assertEquals(Contact.class.getSimpleName(), find.get(0).getType());
			assertEquals(Contact.class.getSimpleName(), find.get(1).getType());
		}
	}

	private Contact create(String name) {
		return new Contact(name, name, name);
	}

}
